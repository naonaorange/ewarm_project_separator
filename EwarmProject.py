import sys
import os
from pathlib import Path
import shutil

import xml.etree.ElementTree as ET

class EwarmProject:
        def __init__(self):
            self.rootDirPath = Path('')
            self.wsDirPath = Path('')

        def SetLogList(self, log):
            self.logList = log
    
        def FindPathFromEwp(self):
            self.ewwPath = list(self.wsDirPath.glob("*.eww"))[0]
            self.ewpPath = list(self.wsDirPath.glob("*.ewp"))[0]
            tree = ET.parse(self.ewpPath)
            root = tree.getroot()

            pathList = []
            for i in root.iter('state'):
                    text = i.text
                    if type(text) == type('string'):
                            if text.find('$PROJ_DIR$') >= 0:
                                text = text.replace('$PROJ_DIR$/', '')
                                #text = text.replace('/', '\\')
                                absPath = self.wsDirPath / text 

                                #ファイルの場合
                                if os.path.isfile(str(absPath)) == True:
                                    pathList.append(text)
                                #フォルダの場合
                                elif os.path.isdir(str(absPath)) == True:
                                    #フォルダ内のファイルをすべて追加する
                                    pl = absPath.glob('./*')
                                    for p in pl:
                                        if os.path.isfile(str(p)) == True:
                                            rp = os.path.relpath(p, self.wsDirPath)
                                            pathList.append(rp)
                                #存在しない場合
                                else:
                                    self.logList.append('[DEBUG] The path doesnt found : ' + str(text))

                                
            return pathList