# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.richtext

###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1 ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"EWARM Project Separator", pos = wx.DefaultPosition, size = wx.Size( 500,500 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Select the root directory", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )

		self.m_staticText1.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer2.Add( self.m_staticText1, 0, wx.ALL, 5 )

		self.m_dirPickerRoot = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE )
		bSizer2.Add( self.m_dirPickerRoot, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Select the eww file", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )

		self.m_staticText2.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer2.Add( self.m_staticText2, 0, wx.ALL, 5 )

		self.m_filePickerEww = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer2.Add( self.m_filePickerEww, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer1.Add( bSizer2, 1, wx.EXPAND, 5 )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Select the output directory", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		self.m_staticText3.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer4.Add( self.m_staticText3, 0, wx.ALL, 5 )

		self.m_dirPickerOutput = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE )
		bSizer4.Add( self.m_dirPickerOutput, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer1.Add( bSizer4, 1, wx.EXPAND, 5 )

		bSizer5 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"Log", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		self.m_staticText4.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer5.Add( self.m_staticText4, 0, wx.ALL, 5 )

		self.m_richTextLog = wx.richtext.RichTextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
		bSizer5.Add( self.m_richTextLog, 4, wx.EXPAND |wx.ALL, 5 )

		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"Log setting", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )

		self.m_staticText5.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer5.Add( self.m_staticText5, 0, wx.ALL, 5 )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_checkBoxLogInfo = wx.CheckBox( self, wx.ID_ANY, u"INFO", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxLogInfo.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer6.Add( self.m_checkBoxLogInfo, 0, wx.ALL, 5 )

		self.m_checkBoxLogDebug = wx.CheckBox( self, wx.ID_ANY, u"DEBUG", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBoxLogDebug.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer6.Add( self.m_checkBoxLogDebug, 0, wx.ALL, 5 )


		bSizer5.Add( bSizer6, 1, wx.EXPAND, 5 )


		bSizer1.Add( bSizer5, 5, wx.EXPAND, 5 )

		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer7 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticTextVersion = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticTextVersion.Wrap( -1 )

		self.m_staticTextVersion.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer7.Add( self.m_staticTextVersion, 0, wx.ALL, 5 )


		bSizer3.Add( bSizer7, 1, wx.ALIGN_LEFT|wx.ALL, 5 )

		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_buttonDebug = wx.Button( self, wx.ID_ANY, u"Debug", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_buttonDebug.Hide()

		bSizer8.Add( self.m_buttonDebug, 0, wx.ALIGN_RIGHT|wx.EXPAND, 5 )

		self.m_buttonStart = wx.Button( self, wx.ID_ANY, u"Start", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer8.Add( self.m_buttonStart, 0, wx.ALIGN_RIGHT|wx.EXPAND, 5 )


		bSizer3.Add( bSizer8, 1, wx.ALIGN_RIGHT|wx.EXPAND|wx.RIGHT, 5 )


		bSizer1.Add( bSizer3, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_dirPickerRoot.Bind( wx.EVT_DIRPICKER_CHANGED, self.m_dirPickerRootOnDirChanged )
		self.m_filePickerEww.Bind( wx.EVT_FILEPICKER_CHANGED, self.m_filePickerEwwOnFileChanged )
		self.m_dirPickerOutput.Bind( wx.EVT_DIRPICKER_CHANGED, self.m_dirPickerOutputOnDirChanged )
		self.m_buttonDebug.Bind( wx.EVT_BUTTON, self.m_buttonDebugOnButtonClick )
		self.m_buttonStart.Bind( wx.EVT_BUTTON, self.m_buttonStartOnButtonClick )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def m_dirPickerRootOnDirChanged( self, event ):
		event.Skip()

	def m_filePickerEwwOnFileChanged( self, event ):
		event.Skip()

	def m_dirPickerOutputOnDirChanged( self, event ):
		event.Skip()

	def m_buttonDebugOnButtonClick( self, event ):
		event.Skip()

	def m_buttonStartOnButtonClick( self, event ):
		event.Skip()


