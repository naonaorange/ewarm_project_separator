import os
import shutil
from pathlib import Path
import distutils.dir_util

from EwarmProject import EwarmProject

class EwarmProjectSeparator():
    def __init__(self):
        self.inputEp = EwarmProject()
        self.outputEp = EwarmProject()

        self.logList = []
        self.inputEp.SetLogList(self.logList)
        self.outputEp.SetLogList(self.logList)

    def SetRootDirPath(self, path):
        self.inputEp.rootDirPath = Path(path)
        self.logList.append('[INFO] Success to set the root directory.')

    def SetEwwPath(self, path):
        self.inputEp.wsDirPath = Path(path).parents[0]
        self.logList.append('[INFO] Success to set the eww file.')

    def SetOutputDirPath(self, path):
        self.outputEp.rootDirPath = Path(path)
        self.logList.append('[INFO] Success to set the output directory.')

    def Debug(self):
        pass

    def Start(self):
        if self.IsSetupOk() == False:
            return
        
        self.inputEp.SetLogList(self.logList)
        self.outputEp.SetLogList(self.logList)

        #OutputのWS Pathを作成
        rp = os.path.relpath(self.inputEp.wsDirPath, self.inputEp.rootDirPath)
        self.outputEp.wsDirPath = self.outputEp.rootDirPath / rp
    
        #WS Dir内のファイルをすべてコピー
        #shutil.copytree(self.inputEp.wsDirPath, self.outputEp.wsDirPath)
        distutils.dir_util.copy_tree(str(self.inputEp.wsDirPath), str(self.outputEp.wsDirPath))

        pathList = self.inputEp.FindPathFromEwp()
        for p in pathList:
            i = self.inputEp.wsDirPath / p
            o = self.outputEp.wsDirPath / p
            self.CopyFile(i, o)

        self.logList.append('[INFO] ###################################')
        self.logList.append('[INFO] Finish separating the EWARM project')
        self.logList.append('[INFO] ###################################')
        
    def IsSetupOk(self):
        r = False
        if str(self.inputEp.rootDirPath) != '':
            if str(self.inputEp.wsDirPath) != '':
                if str(self.inputEp.rootDirPath) != '':
                    r = True
        return r

    def CopyFile(self, fromPath, toPath):
        dirPath = toPath.parents[0]
        if not os.path.exists(dirPath):
            os.makedirs(dirPath)
        shutil.copyfile(fromPath, toPath)


                    